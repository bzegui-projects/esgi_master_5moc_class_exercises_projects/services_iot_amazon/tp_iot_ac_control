# aws_iam_role iot_role
resource "aws_iam_role" "iot_role" {
  name               = "iot_role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# aws assume role policy document
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["iot.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

# aws_iam_role_policy iam_policy_for_dynamodb
resource "aws_iam_role_policy" "iam_policy_for_dynamodb" {
  name = "iot_role_policy"
  role = aws_iam_role.iot_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:PutItem",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

# aws_iam_role_policy iam_policy_for_timestream_writing
resource "aws_iam_role_policy" "iam_policy_for_timestream_write" {
  name = "iot_role_policy"
  role = aws_iam_role.iot_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "timestream:WriteRecords",
        "Resource": aws_timestreamwrite_table.temperaturesensor.arn,
      },
      {
        "Effect": "Allow",
        "Action": "timestream:DescribeEndpoints",
        "Resource": "*",
      }
    ]
  })
}

# aws_iam_role lambda_role
resource "aws_iam_role" "lambda_role" {
  name               = "lambda_role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_lambda.json
}

# lambda role policy document
data "aws_iam_policy_document" "assume_role_lambda" {
  statement {
    effect = "Allow"
    sid = ""

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

# aws_iam_role_policy iam_policy_for_timestream_reading for Lambda
resource "aws_iam_role_policy" "iam_policy_for_timestream_select" {
  name = "lambda_role_policy"
  role = aws_iam_role.lambda_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "timestream:Select",
        "Resource": aws_timestreamwrite_table.temperaturesensor.arn,
      },
      {
        "Effect": "Allow",
        "Action": "timestream:DescribeEndpoints",
        "Resource": "*",
      }
    ]
  })
}

# aws_iam_role_policy iam_policy_for_iot_publishing for Lambda




###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iam_role_policy" "iam_policy_for_logs" {
#  name = "cloudwatch_policy"
#  role = aws_iam_role.iot_role.id
#
#  policy = <<EOF
#{
#        "Version": "2012-10-17",
#        "Statement": [
#            {
#                "Effect": "Allow",
#                "Action": [
#                    "logs:CreateLogGroup",
#                    "logs:CreateLogStream",
#                    "logs:PutLogEvents",
#                    "logs:PutMetricFilter",
#                    "logs:PutRetentionPolicy"
#                 ],
#                "Resource": [
#                    "*"
#                ]
#            }
#        ]
#    }
#EOF
#}


###########################################################################################
# Enable the following resources to enable logging for your Lambda function (helps debug)
###########################################################################################

#resource "aws_cloudwatch_log_group" "example" {
#  name              = "/aws/lambda/${aws_lambda_function.ac_control_lambda.function_name}"
#  retention_in_days = 14
#}
#
#resource "aws_iam_policy" "lambda_logging" {
#  name        = "lambda_logging"
#  path        = "/"
#  description = "IAM policy for logging from a lambda"
#
#  policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Action": [
#        "logs:CreateLogGroup",
#        "logs:CreateLogStream",
#        "logs:PutLogEvents"
#      ],
#      "Resource": "arn:aws:logs:*:*:*",
#      "Effect": "Allow"
#    }
#  ]
#}
#EOF
#}
#
#resource "aws_iam_role_policy_attachment" "lambda_logs" {
#  role       = aws_iam_role.lambda_role.name
#  policy_arn = aws_iam_policy.lambda_logging.arn
#}
