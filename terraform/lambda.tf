# aws_lambda_function to control air conditioner
resource "aws_lambda_function" "ac_control_lambda" {
  filename      = "files/empty_package.zip"
  function_name = "ac_control_lambda"
  role          = aws_iam_role.lambda_role.arn
  handler       = "ac_control_lambda.lambda_handler"
  runtime       = "python3.8"
}

# aws_cloudwatch_event_rule scheduled action every minute
resource "aws_cloudwatch_event_rule" "every_one_minute" {
  name        = "cloudwatch_lambda_event_rule"
  description = "cloudwatch_lambda_event_rule"
  schedule_expression = "rate(1 minute)"
}

# aws_cloudwatch_event_target to link the schedule event and the lambda function
resource "aws_cloudwatch_event_target" "cloudwatch_lambda_event_target" {
  rule = aws_cloudwatch_event_rule.every_one_minute.id
  target_id = "lambda"
  arn = aws_lambda_function.ac_control_lambda.arn
}

# aws_lambda_permission to allow CloudWatch (event) to call the lambda function
resource "aws_lambda_permission" "ac_control_lambda_permission" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ac_control_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.every_one_minute.arn
}