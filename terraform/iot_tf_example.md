# aws_iot_certificate
resource "aws_iot_certificate" "cert" {
  active = true
}

# aws_iot_policy
resource "aws_iot_policy" "pubsub" {
  name = "PubSubToAnyTopic"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
        "Version": "2012-10-17",
        "Statement": [
            {
            "Action": [
                "iot:Connect"
            ],
            "Effect": "Allow",
            "Resource": "*"
            },
            {
            "Action": [
                "iot:Receive"
            ],
            "Effect": "Allow",
            "Resource": "*"
            },
            {
            "Action": [
                "iot:Publish"
            ],
            "Effect": "Allow",
            "Resource": "*"
            },
            {
            "Action": [
                "iot:Subscribe"
            ],
            "Effect": "Allow",
            "Resource": "*"
            }
        ]
    })
}

# aws_iot_policy_attachment
resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}

# aws_iot_thing
resource "aws_iot_thing" "example" {
  name = "example"

  attributes = {
    First = "examplevalue"
  }
}

# aws_iot_thing_principal_attachment
resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.example.name
}

# aws_iot_endpoint
data "aws_iot_endpoint" "example" {
    endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic dynamoDB table
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "Temperature"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "Id"

  attribute {
    name = "Id"
    type = "S"
  }
}

# aws_iot_topic_rule dynamoDB
resource "aws_iot_topic_rule" "rule" {
  name        = "SelectRule"
  description = "Select from test topic"
  enabled     = true
  sql         = "SELECT *, timestamp() as timestamp FROM 'test/topic'"
  sql_version = "2016-03-23"

  dynamodbv2 {
    put_item {
      table_name = "Temperature"
    }
    role_arn = aws_iam_role.iot_role.arn
  } 
}

# aws assume role policy document
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["iot.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

# aws iot role
resource "aws_iam_role" "iot_role" {
  name               = "iot_role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# aws iam role policy for iot_role
resource "aws_iam_role_policy" "iot_role_policy" {
  name = "iot_role_policy"
  role = aws_iam_role.iot_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:PutItem",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

# aws iam role policy for s3
resource "aws_iam_role_policy" "iot_role_s3policy" {
  name = "iot_role_policy"
  role = aws_iam_role.iot_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:PutObject",
          "s3:DeleteObject",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

//buckets
resource "aws_s3_bucket" "temperature" {
  bucket = "temperature"
  force_destroy = true
}

# aws_iot_topic_rule dynamoDB
resource "aws_iot_topic_rule" "temp_rule" {
  name        = "ConditionalSelectTempRule"
  description = "ConditionalSelectTempRule"
  enabled     = true
  sql         = "SELECT *, timestamp() as timestamp FROM 'test/topic' where temperature > 35"
  sql_version = "2016-03-23"
  

  s3 {
    bucket_name = aws_s3_bucket.temperature.bucket
    key         = "test-topic/$${timestamp()}.txt"   
    role_arn    = aws_iam_role.iot_role.arn
  }
}

# aws_iot_topic_rule s3



###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}

